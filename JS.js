$("#btn-submit-data").click(function() {
  // Lấy dữ liệu từ các trường nhập liệu
  var firstName = $("#inp-first-name").val();
  var lastName = $("#inp-last-name").val();
  var age = $("#inp-age").val();

  // Kiểm tra dữ liệu
  if (firstName != "" && lastName != "" && age != "") {
    if (isNaN(age) || age <= 0 || age >= 200) {
      console.log("Dữ liệu không hợp lệ");
    } else {
      // Ghi dữ liệu vào console log
      console.log("First Name: " + firstName);
      console.log("Last Name: " + lastName);
      console.log("Age: " + age);

      // Nối dữ liệu vào vùng log
      $("#p-html-log").append("<br> >fistname : " + firstName + "<br> >lastname : " + lastName + "<br> >age : " + age);
    }
  } else {
    console.log("Dữ liệu không hợp lệ");
  }
});

// Xử lý sự kiện khi người dùng nhấn nút Clear HTML Log
$("#btn-clear-log").click(function() {
  // Xóa vùng log
  $("#p-html-log").html(">");
});